//
//  Constants.swift
//  Moveo Assignment
//
//  Created by Roei Baruch on 09/01/2018.
//  Copyright © 2018 Roei Baruch. All rights reserved.
//

import Foundation

struct Constants {
    static let GITHUB_REQUEST_AOUTH = "https://api.github.com/user/repos"

    static let GITHUB_REQUEST_URL = "https://api.github.com/search/repositories"
    static let GET_LAST_MONTH = dateWanted(date: .month)
    static let GET_LAST_WEEK = dateWanted(date: .weekOfYear)
    static let GET_LAST_DAY  = dateWanted(date: .day)
    
    static let LAST_MONTH_TITLE = "Last month repositories"
    static let LAST_WEEK_TITLE = "Last week repositories"
    static let LAST_DAY_TITLE = "Last day repositories"
    
    static let ERROR_API = "Please try again"
}


func dateWanted(date: Calendar.Component) -> String{
    let lastWeekDate = Calendar.current.date(byAdding: date, value: -1, to: Date())!
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    let lastWeekDateString = dateFormatter.string(from: lastWeekDate)
    return "created:>\(lastWeekDateString)"
}
