//
//  ViewController.swift
//  Moveo Assignment
//
//  Created by Roei Baruch on 09/01/2018.
//  Copyright © 2018 Roei Baruch. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var titleLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

//

    }
 
    @IBAction func toRepo(_ sender: Any) {
        let secondViewController:HomeViewController = HomeViewController()
        
        self.present(secondViewController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

