//
//  AlertHelper.swift
//  Moveo Assignment
//
//  Created by Roei Baruch on 10/01/2018.
//  Copyright © 2018 Roei Baruch. All rights reserved.
//

import UIKit

class AlertHelper {

    
    // Can't init is singleton
    private init() { }
    
    // MARK: Shared Instance
    
    static let sharedInstance = AlertHelper()
    
    // MARK: Local Variable
    class func ShowAlert(title: String, message: String, in vc: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    class func showAlertOnViewController(
        targetVC: UIViewController,
        title: String,
        message: String)
    {
        
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert)
        let okButton = UIAlertAction(
            title:"OK",
            style: UIAlertActionStyle.default,
            handler:
            {
                (alert: UIAlertAction!)  in
        })
        alert.addAction(okButton)
        targetVC.present(alert, animated: true, completion: nil)
    }
    
}
