//
//  DataManager.swift
//  Moveo Assignment
//
//  Created by Roei Baruch on 09/01/2018.
//  Copyright © 2018 Roei Baruch. All rights reserved.
//

import UIKit
import Alamofire

class DataManager {
    

    private init() { }

    // MARK: Shared Instance
    
    static let sharedInstance = DataManager()
    
    func FatchDataWithPageNumberAndUrl(url: String, page: NSInteger, completionHandler:@escaping ([Model],(NSInteger)) -> Void, failure:@escaping (Error) -> Void){
        var models = [Model]()

        GitApi.sharedInstance.requestGitRepoByCreatedTime(time: url, page: page, success: { (result) in
            if let resData = result.0["items"].arrayObject {
                for item in resData{
                    let model = Model()
                    if let itemDic = item as? [String:AnyObject]{
                        model.username = itemDic["owner"]?["login"] as? String ?? ""
                        model.des = itemDic["description"] as? String ?? ""
                        model.avatar_url = itemDic["owner"]?["avatar_url"] as? String ?? ""
                        let stars = itemDic["stargazers_count"] as? NSInteger ?? 0
                        model.stars = "\(stars)"
                        model.url = itemDic["html_url"] as? String ?? ""
                        model.language = itemDic["language"] as? String ?? ""
                        model.created = itemDic["created_at"] as? String ?? ""
                        let forks = itemDic["forks"] as? NSInteger ?? 0
                        model.forks = "\(forks)"
                        let id = itemDic["id"] as? NSInteger ?? 0
                        model.id = "\(id)"
                        models.append(model)
                    }
                }
                completionHandler(models,result.1)
            }
        }) { (e) in
            print("error\(e)")
            failure(e)
        }
        
    }
}
