//
//  ModalViewController.swift
//  Moveo Assignment
//
//  Created by Roei Baruch on 10/01/2018.
//  Copyright © 2018 Roei Baruch. All rights reserved.
//

import UIKit

class ModalViewController: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var languageLbl: UILabel?
    @IBOutlet weak var createdLbl: UILabel?
    @IBOutlet weak var urlLbl: UILabel?
    @IBOutlet weak var forksLbl: UILabel?
    
    @IBOutlet weak var dismiss: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func fillModalWithModel(model:Model){
        self.titleLbl.text = model.username
        self.languageLbl?.text = "Language: \(model.language)"
        self.createdLbl?.text = "Created: \(model.created)"
        self.forksLbl?.text = "Forks: \(model.forks)"
        self.urlLbl?.text = "url: \(model.url)"
    }
}
