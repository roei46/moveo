//
//  File.swift
//  Moveo Assignment
//
//  Created by Roei Baruch on 09/01/2018.
//  Copyright © 2018 Roei Baruch. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class GitApi {
    
    // Can't init is singleton
    private init() { }
    
    // MARK: Shared Instance
    
    static let sharedInstance = GitApi()
    
    // MARK: Local Variable
    
    func requestGitRepoByCreatedTime(time:String, page:NSInteger, success:@escaping (JSON,NSInteger) -> Void, failure:@escaping (Error) -> Void){
        Alamofire.request(
            Constants.GITHUB_REQUEST_URL,
            parameters:[
                "q"     : time,
                "sort" : "stars",
                "order"    : "desc",
                "page" : page
            ],headers:["Authorization":"token 18845170c1ac48e2a734f5734b37d809df9e08db"]).responseJSON { (responseObject) -> Void in
                if responseObject.result.isSuccess {
                    if((responseObject.response?.statusCode) == 200){
                        
                        let resJson = JSON(responseObject.result.value!)
                        if let header = responseObject.response?.allHeaderFields["Link"] as? String{
                            let LastPageNumber = self.getFirtLastPageFromHeader(header: header)
                            success(resJson,LastPageNumber)
                        }else{
                            success(resJson,0)

                        }
                    }else{
                        let error = responseObject.result.value as? NSDictionary
                        let errorMessage = error?.object(forKey: "message") as! String
                        print(errorMessage)
                        let errorTemp = NSError(domain:errorMessage, code:(responseObject.response?.statusCode)!, userInfo:nil)
                        
                        failure(errorTemp)
                    }
                }
                if responseObject.result.isFailure {
                    let error : Error = responseObject.result.error!
                    failure(error)
                }
        }
    }
    
    
    
    func getFirtLastPageFromHeader(header:String) -> NSInteger{
        var lastPageNumber: NSInteger = 0
        let headerArr = header.components(separatedBy: ",")
        for word in headerArr{
            if word.lowercased().range(of:"last") != nil {
                let regCode:String = (word.components(separatedBy: "page=").last?.components(separatedBy: "&").first)!
                lastPageNumber = NSInteger(regCode)!
            }
        }
        return lastPageNumber
        
    }
}

