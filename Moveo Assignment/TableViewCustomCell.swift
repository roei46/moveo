//
//  TableViewCustomCell.swift
//  Moveo Assignment
//
//  Created by Roei Baruch on 14/01/2018.
//  Copyright © 2018 Roei Baruch. All rights reserved.
//

import UIKit
import SDWebImage

protocol MyCellDelegate {
    func btnCloseTapped(cell: TableViewCustomCell)
}

class TableViewCustomCell: UITableViewCell {
    
    @IBOutlet weak var addBtn: UIButton!

    var delegate : MyCellDelegate?

    @IBOutlet weak var userName: UILabel?
    @IBOutlet weak var des: UILabel?
    @IBOutlet weak var numberOfStars: UILabel?
    @IBOutlet weak var avater: UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addBtn.layer.cornerRadius = 5
    }

    func fillCellWithModel(model:Model){
        
        
        self.userName?.text = model.username
        self.des?.text = model.des
        self.numberOfStars?.text = "Stars: \(model.stars)"
        avater?.sd_setImage(with: URL(string: model.avatar_url), placeholderImage: UIImage(named: "placeholder.png"))
        avater?.sd_setShowActivityIndicatorView(true)
        avater?.sd_setIndicatorStyle(.gray)
        
        
    }
    
    @IBAction func addToFavorite(_ sender: Any) {

        if let _ = delegate {
            delegate?.btnCloseTapped(cell: self)
        }
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
