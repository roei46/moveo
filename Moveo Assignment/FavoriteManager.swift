//
//  FavoriteManager.swift
//  Moveo Assignment
//
//  Created by Roei Baruch on 11/01/2018.
//  Copyright © 2018 Roei Baruch. All rights reserved.
//

import UIKit
import CoreData

class FavoriteManager {
    
   // var models = [Model]()
    var models: [NSManagedObject] = []

    
    private init() { }
    
    // MARK: Shared Instance
    
    static let sharedInstance = FavoriteManager()
    
    func fromNSManagedObjectToModel(object: NSManagedObject) -> (Model){
        let item = Model()

        item.created = object.value(forKey: "created") as? String ?? ""
        item.des = object.value(forKey: "des") as? String ?? ""
        item.username = object.value(forKey: "username") as? String ?? ""
        item.avatar_url = object.value(forKey: "avatar_url") as? String ?? ""
        item.forks = object.value(forKey: "username") as? String ?? ""
        item.language = object.value(forKey: "language") as? String ?? ""
        item.stars = object.value(forKey: "stars") as? String ?? ""
        item.url = object.value(forKey: "url") as? String ?? ""
        item.id = object.value(forKey: "id") as? String ?? ""

        return item
    }
    
    func deleteFromFavorite(item: NSManagedObject){
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext

        managedContext.delete(item)
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
        
    }
    
    func isExist(id: String) -> Bool {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return false
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Favorite")
        fetchRequest.predicate = NSPredicate(format: "id = \(id)", argumentArray: nil)
        
        let res = try! managedContext.fetch(fetchRequest)
        return res.count > 0 ? true : false
    }
    
    func extractDataFromCore(completionHandler:@escaping ([NSManagedObject]) -> Void, failure:@escaping (Error) -> Void){
        
        var models: [NSManagedObject] = []
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Favorite")
        
        do {
        
            models = try managedContext.fetch(fetchRequest)
            completionHandler(models)
           // self.tblView.reloadData()
            
            
        } catch let error as NSError {
            failure(error.userInfo as! Error)
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    func saveFavorit(model: Model) ->Bool{
        
         let appDelegate =
            UIApplication.shared.delegate as? AppDelegate
        
        let managedContext =
            appDelegate?.persistentContainer.viewContext
        
        if isExist(id:model.id){

        return false
        }else{
        let entity =
                NSEntityDescription.entity(forEntityName: "Favorite",
                                           in: managedContext!)!
            
        let favorite = NSManagedObject(entity: entity,
                                           insertInto: managedContext)
        favorite.setValue(model.username, forKeyPath: "username") 
        favorite.setValue(model.des, forKeyPath: "des")
        favorite.setValue(model.stars, forKeyPath: "stars")
        favorite.setValue(model.language, forKeyPath: "language")
        favorite.setValue(model.forks, forKeyPath: "forks")
        favorite.setValue(model.created, forKeyPath: "created")
        favorite.setValue(model.url, forKeyPath: "url")
        favorite.setValue(model.avatar_url, forKeyPath: "avatar_url")
        favorite.setValue(model.id, forKeyPath: "id")
        
        do {
            try managedContext?.save()
            models.append(favorite)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
            return false
            }
        }
        return true
    }

}
