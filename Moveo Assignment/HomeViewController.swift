//
//  HomeViewController.swift
//  Moveo Assignment
//
//  Created by Roei Baruch on 09/01/2018.
//  Copyright © 2018 Roei Baruch. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

enum TabItems : Int {
    case LastMonth = 0
    case LastWeek = 1
    case LastDay = 2
}

class HomeViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension HomeViewController: UITabBarControllerDelegate {
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
    if let tabItem = TabItems.init(rawValue: item.tag) {
        switch tabItem {
        case .LastMonth:
            RepositoryViewController.createdString = Constants.GET_LAST_MONTH
            RepositoryViewController.titleString = Constants.LAST_MONTH_TITLE
        case .LastWeek:
            RepositoryViewController.createdString = Constants.GET_LAST_WEEK
            RepositoryViewController.titleString = Constants.LAST_WEEK_TITLE
        case .LastDay:
            RepositoryViewController.createdString = Constants.GET_LAST_DAY
            RepositoryViewController.titleString = Constants.LAST_DAY_TITLE
            }
        }
    }
}

