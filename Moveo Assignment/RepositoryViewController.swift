//
//  BaseViewController.swift
//  Moveo Assignment
//
//  Created by Roei Baruch on 12/01/2018.
//  Copyright © 2018 Roei Baruch. All rights reserved.
//

import UIKit
import SVProgressHUD

class RepositoryViewController: UIViewController, MyCellDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var models = [Model]()
    
    static var createdString: String = Constants.GET_LAST_MONTH
    static var titleString: String = Constants.LAST_MONTH_TITLE
    static var sharedVariable: Bool = false

    var lastPageNumber = NSInteger()
    var page : NSInteger = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        let nib = UINib(nibName: "TableViewCustomCell", bundle: nil)
        
        tableView.register(nib, forCellReuseIdentifier: "Cell1")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.networkCallWithPage(page: page)
        self.tabBarController?.navigationItem.title = RepositoryViewController.titleString

    }
    
    func btnCloseTapped(cell: TableViewCustomCell) {
        //Get the indexpath of cell where button was tapped
        let indexPath = self.tableView.indexPath(for: cell)
        print(indexPath!.row)
        if indexPath != nil {
            if (!FavoriteManager.sharedInstance.saveFavorit(model: self.models[(indexPath?.row)!])){
                AlertHelper.ShowAlert(title: "Ops", message: "Failed saving Repository", in: self)
            }else{
                AlertHelper.ShowAlert(title: "Success", message: "Repository saved", in: self)
                
            }
        }
    }
    
    func networkCallWithPage(page:NSInteger){
        SVProgressHUD.show()
        DataManager.sharedInstance.FatchDataWithPageNumberAndUrl(url:RepositoryViewController.createdString, page:page,completionHandler: { (result) in
            self.models.append(contentsOf: result.0)
            self.lastPageNumber = result.1
            
            if self.models.count > 0 {
                self.tableView.reloadData()
            }else{
                
                let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
                label.center = CGPoint(x: 160, y: 285)
                label.textAlignment = .center
                label.text = "No result!"
                self.view.addSubview(label)

            }
            SVProgressHUD.dismiss()
            
        }, failure: { (e) in
            AlertHelper.ShowAlert(title: e.localizedDescription, message: Constants.ERROR_API, in: self)
            SVProgressHUD.dismiss()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension RepositoryViewController:  UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! TableViewCustomCell

        cell.delegate = self

        if indexPath.row == self.models.count - 1 {
            
            if page <= lastPageNumber{
                page += 1
                self.networkCallWithPage(page: page)
            }
        }
        
        cell.fillCellWithModel(model: self.models[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let modalVC : ModalViewController = storyboard.instantiateViewController(withIdentifier: "Modal") as! ModalViewController
        present(modalVC, animated: true, completion: {
            modalVC.fillModalWithModel(model: self.models[indexPath.row])
        })
    }
}
