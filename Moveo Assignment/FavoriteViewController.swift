//
//  favoriteViewController.swift
//  Moveo Assignment
//
//  Created by Roei Baruch on 11/01/2018.
//  Copyright © 2018 Roei Baruch. All rights reserved.
//

import UIKit
import CoreData

class favoriteViewController: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    
    var models: [NSManagedObject] = []
    
    var manageObjectContext = NSManagedObjectContext()
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var filteredTableData = [Model]()

    override func viewDidLoad() {
        super.viewDidLoad()
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Candies"
        self.tblView.tableHeaderView = searchController.searchBar
        definesPresentationContext = true
        
        self.tabBarController?.navigationItem.title = "FAVORITES"
        
        let nib = UINib(nibName: "TableViewCustomCell", bundle: nil)
        
        tblView.register(nib, forCellReuseIdentifier: "Cell1")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        FavoriteManager.sharedInstance.extractDataFromCore(completionHandler: { (result) in
            self.models = result
            self.tblView.reloadData()

        }) { (e) in
            AlertHelper.ShowAlert(title: "Ops", message: e.localizedDescription, in: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        self.filteredTableData.removeAll()
        for item in self.models{
            let model = FavoriteManager.sharedInstance.fromNSManagedObjectToModel(object: item)
            
            self.filteredTableData.append(model)
        }
        
        filteredTableData = self.filteredTableData.filter({( candy : Model) -> Bool in
            return candy.username.lowercased().contains(searchText.lowercased())
        })
        
        tblView.reloadData()
    }
}

extension favoriteViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}

extension favoriteViewController:  UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.isFiltering() {
            return self.filteredTableData.count
        }
            return self.models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! TableViewCustomCell
        
        cell.addBtn.isHidden = true
        let model: Model

        if self.isFiltering(){
            model =  self.filteredTableData[indexPath.row]
            
        }else{
            let item = self.models[indexPath.row]
            
            model = FavoriteManager.sharedInstance.fromNSManagedObjectToModel(object: item)
        }
        
        cell.fillCellWithModel(model: model)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            FavoriteManager.sharedInstance.deleteFromFavorite(item: self.models[indexPath.row])
            
            self.models.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        self.tblView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let modalVC : ModalViewController = storyboard.instantiateViewController(withIdentifier: "Modal") as! ModalViewController
        present(modalVC, animated: true, completion: {
            let item = self.models[indexPath.row]
            
            let model = FavoriteManager.sharedInstance.fromNSManagedObjectToModel(object: item)
            modalVC.fillModalWithModel(model: model)
        })
    }
}
